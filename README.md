# Tool to populate Incubateur's services databases

## Installation
You can use either a global install: `npm i -g .` and then run `icb` or npx `npx icb`.

Use the `--help` flag to get full command informations: `icb --help`.

## Stack
- Table as the initial data source
- SearchEngine to hydrate `/services` page
- API as the new data source

## Environment variables
| NAME | LOCATION |
| --- | --- |
| SEARCH_ENGINE_MASTER_KEY | Generated on searchEngine start |
| SEARCH_ENGINE_HOST | URL to searchEngine instance |
| API_URL | URL to api instance |
| API_TOKEN | API token, needs API rights |
| TABLE_API_KEY | Available in your Table account |
| SERVICE_DATABASE_ID | Available in table API documentation |
| SERVICE_DATABASE_NAME | Sub table of Service project |
| ANSM_DATABASE_ID | Available in table API documentation |
| ANSM_MAIN_DATABASE_NAME | Sub table of ANSM project |
| ANSM_AUTHORITY_DATABASE_NAME | Sub table of ANSM project |
| ANSM_NEEDS_DATABASE_NAME | Sub table of ANSM project |
| ANSM_SOLUTIONS_DATABASE_NAME | Sub table of ANSM project |

## Troubleshooting
Search engine enqueues jobs thus it can lead to read while it is still processing previous command.
If it displays 0 documents after creation, please run the same command in read mode to double check.
