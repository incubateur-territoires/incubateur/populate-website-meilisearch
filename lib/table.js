import Airtable from 'airtable';

export const {
    SERVICE_DATABASE_ID,
    SERVICE_DATABASE_NAME,
    TABLE_API_KEY,
} = process.env;
export const TABLE_ENDPOINT = 'https://api.airtable.com';

export function getDatabase(id) {
    Airtable.configure({
        endpointUrl: TABLE_ENDPOINT,
        apiKey: TABLE_API_KEY
    });

    return Airtable.base(id);
}

export async function selectRecords({ client, databaseName, options  }) {
    return client(databaseName).select(options).all();
}

export async function populateData({ client, records, decisionTree }) {
    const keyKeys = Object.keys(decisionTree);
    let populatedRecords = [];

    for (const record of records) {
        const entries = Object.entries(record);
        let finalRecord = {};

        for (const [key, value] of entries) {
            if (!keyKeys.includes(key)) {
                finalRecord = {
                    ...finalRecord,
                    [key]: value,
                }
            } else {
                const database = client(decisionTree[key].databaseName);
                const output = await Promise.all(
                    value.map(async (id) => database.find(id))
                );

                finalRecord = {
                    ...finalRecord,
                    ...decisionTree[key].formatOutput({ key, output }),
                }
            }
        }

        populatedRecords.push(finalRecord);
    }

    console.log(populatedRecords[0]);

    return populatedRecords;
}

export function formatFieldNames({ records, conversionTable }) {
    console.log(records[0]);
    return records.map((record) => Object
        .entries(record)
        .reduce((output, [key, value]) => {
            if (conversionTable[key]) {
                return {
                    ...output,
                    [conversionTable[key]]: value,
                }
            }

            return output;
        }, {}))
}