import { MeiliSearch } from 'meilisearch';
import { logInfo } from './logger.js';

export const {
  SEARCH_ENGINE_HOST,
  SEARCH_ENGINE_MASTER_KEY,
} = process.env;

export function getSearchEngineCollection(index) {
  return getSearchEngineClient().index(index);
}

export function getSearchEngineClient() {
  return new MeiliSearch({
    host: SEARCH_ENGINE_HOST,
    apiKey: SEARCH_ENGINE_MASTER_KEY,
  });
}

export async function createSearchEngineIndex(index) {
  const client = getSearchEngineClient();
  const doesExist = await client.getIndex(index);

  if (!doesExist) {
    return client.createIndex(index, { primaryKey: 'id' });
  }

  logInfo(`Index ${index} already exists in search engine`);
}