import qs from 'qs';

const { API_URL, API_TOKEN } = process.env;

const token = `Bearer ${API_TOKEN}`;

export async function find({ collection, params }) {
    const url = getApiURL({ endpoint: collection, params });
    const requestOptions = getRequestOptions();
    
    const res = await fetch(url, requestOptions);
    const { data } = await res.json();
    return data;
}

export async function create({ collection, payload, params }) {
    const url = getApiURL({ endpoint: collection, params });
    const requestOptions = postRequestOptions({
        body: JSON.stringify(payload),
    });

    const res = await fetch(url, requestOptions);
    const { data } = await res.json();
    return data;
}

export async function updateOne({ collection, record, params }) {
    const url = getApiURL({
        endpoint: `${collection}/${record.id}`,
        params
    });
    const requestOptions = patchRequestOptions({
        body: JSON.stringify(record)
    });

    const res = await fetch(url, requestOptions);
    const { data } = await res.json();
    return data;
}

export async function filterRecordsByExistence({ collection, records, filterField }) {
    const data = await find({ collection });

    return records.reduce(({ newRecords, oldRecords }, record) => {
        const existingRecord = data.find((item) => item[filterField] === record[filterField]);

        if (existingRecord) {
            oldRecords.push({ id: existingRecord.id, ...record });
        } else {
            newRecords.push(record);
        }

        return { newRecords, oldRecords };
    }, { newRecords: [], oldRecords: [] });
}

function getApiURL({ endpoint, params }) {
    const queryParams = params ? `?${qs.stringify(params)}` : '';
    return `${API_URL}/items/${endpoint}${queryParams}`;
}

function getRequestOptions(options) {
    return generateRequestOptions({
        method: 'GET',
        ...options
    });
}

function postRequestOptions(options) {
    return generateRequestOptions({
        method: 'POST',
        ...options
    });
}

function patchRequestOptions(options) {
    return generateRequestOptions({
        method: 'PATCH',
        ...options
    });
}

function generateRequestOptions(options) {
    return {
        headers: {
            "Authorization": token,
            "Content-Type": 'application/json'
        },
        ...options,
    }
}
