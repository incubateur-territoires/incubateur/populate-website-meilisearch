import meow from 'meow';
import ansm, { ANSM_COLLECTION } from '../commands/ansm.js';
import services, { SERVICES_COLLECTION } from '../commands/services.js';
import { logInfo, logWarn, logTitle } from '../lib/logger.js';
import { asyncSpinner } from './spinner.js';
import { createSearchEngineIndex } from './search.js';

const ANSM_COMMAND = 'ansm';
const SERVICE_COMMAND = 'services';
const commands = [ANSM_COMMAND, SERVICE_COMMAND];

export function parseCLI() {
    const cli = meow(...getCLIConfig());
    const [command] = cli.input;

    if (!commands.includes(command)) {
        gracefullyExit(cli);
    }

    return { command, flags: cli.flags };
}

function getCLIConfig() {
    const help = `
        Usage
            <binary> <command> <options>

        Commands
            ansm                Handles ANSM collection manipulation
            services            Handles Services collection manipulation

        Options
            --full, -f          Full process
            --search, -s        Update only search engine
            --read, -r          Get current collection state, default behavior
            --info, -i          Get information on specified command
    `;

    const config = {
        importMeta: import.meta,
        flags: {
            full: {
                type: 'boolean',
                alias: 'f',
            },
            search: {
                type: 'boolean',
                alias: 's',
            },
            read: {
                type: 'boolean',
                alias: 'r',
                default: true,
            },
            info: {
                type: 'boolean',
                alias: 'i',
            }
        }
    }

    return [help, config];
}

export async function runCommand({ command, flags }) {
    await asyncSpinner({
        promise: preCommandExecution,
        message: 'Checking search engine indexes',
    });

    const mode = getMode(flags);
    switch (command) {
        case ANSM_COMMAND:
            await ansm(mode);
        case SERVICE_COMMAND:
            await services(mode);
        default:
            gracefullyExit();
    }
}

function getMode({ full, search, info }) {
    if (full) {
        return 'full';
    }
    if (search) {
        return 'search';
    }
    if (info) {
        return 'info';
    }

    return 'read';
}

function gracefullyExit(cli) {
    logWarn('No valid command provided');
    cli.showHelp();
}

function preCommandExecution() {
    const INDEXES = [ANSM_COLLECTION, SERVICES_COLLECTION];
    return Promise.all(INDEXES.map((index) => createSearchEngineIndex(index)));
}

export function displayInfo(message) {
        logInfo(message);
        process.exit(0);
}

export function displayCommandTitle({ command, mode }) {
    logTitle(`Running ${command} command`);
    logInfo(`-- ${mode} mode --`);
}
