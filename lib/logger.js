import chalk from 'chalk';

export const { log } = console;

export function logTitle(message) {
    return log(chalk.bold.underline(message));
}
export function logInfo(message) {
    return log(chalk.bold.blue(message));
}
export function logWarn(message) {
    return log(chalk.bold.yellow(message));
}
export function logSuccess(message) {
    return log(chalk.bold.green(message));
}
export function logError(message) {
    return log(chalk.bold.red(message));
}