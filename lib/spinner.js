import ora, { oraPromise } from 'ora';

export function startSpinner(message) {
    return ora(message).start();
}

export function asyncSpinner({ promise, message }) {
    return oraPromise(promise, message)
}

export function stopSpinner({ spinner, status }) {
    switch (status) {
        case 'success':
            return spinner.succeed();
        case 'error':
            return spinner.fail();
        case 'warn':
            return spinner.warn();
        case 'info':
            return spinner.info();
        default:
            return spinner.stop();
    }
}