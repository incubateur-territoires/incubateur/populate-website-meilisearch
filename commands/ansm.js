import {
    formatFieldNames,
    getDatabase,
    populateData,
    selectRecords
} from "../lib/table.js";
import { create, filterRecordsByExistence, find, updateOne } from "../lib/api.js";
import { logInfo, logSuccess } from "../lib/logger.js";
import { getSearchEngineCollection } from "../lib/search.js";
import { asyncSpinner, startSpinner, stopSpinner } from "../lib/spinner.js";
import { displayInfo, displayCommandTitle } from "../lib/cli.js";
import { slugify } from '../lib/slugify.js';

export default async function command(mode) {
    if (mode === 'info') {
        displayInfo(INFO);
    }

    displayCommandTitle({ command: COMMAND, mode });

    if (mode === 'full') {
        const tableRecords = await asyncSpinner({
            promise: fetchTable(),
            message:'Fetching records from table'
        });
        logSuccess(`Fetched ${tableRecords.length} records.`);
        console.log(tableRecords)

        const spinner = startSpinner('Formatting records');
        const records = formatFieldNames({
            records: tableRecords,
            conversionTable: CONVERSION_TABLE
        });
        stopSpinner({ spinner, status: 'success' });


        const recordsWithSlug = records.map((record) => {
            const local_authority = record.local_authority;
            const slug = slugify(local_authority);

            return {
                ...record,
                slug,
            }
        });

        await asyncSpinner({
            promise: saveToAPI(recordsWithSlug),
            message: 'Creating entries in API'
        });
    }

    const client = getSearchEngineCollection(ANSM_COLLECTION);
    const updatedRecords = await find({ collection: ANSM_COLLECTION });

    logInfo(`API documents number: ${updatedRecords.length}`);

    if (mode === 'search' || mode === 'full') {
        await asyncSpinner({
            promise: client.deleteAllDocuments(),
            message: 'Delete all documents from search engine',
        });

        await asyncSpinner({ 
            promise: client.addDocuments(updatedRecords),
            message: 'Creating fresh documents in search engine',
        });
    }

    const { numberOfDocuments } = await client.getStats();

    logInfo(`Search engine documents number: ${numberOfDocuments}`);
    process.exit(0);
}

async function fetchTable() {
    const client = getDatabase(ANSM_DATABASE_ID);

    const options = {
        fields: FIELDS_WHITELIST,
        filterByFormula: CLOSED_STATUS,
    }
    
    const records = await selectRecords({ client, databaseName: ANSM_MAIN_DATABASE_NAME, options });
    return populateData({ client, records: sanitizeRecords(records), decisionTree: DATA_DECISION_TREE });
}

async function saveToAPI(records) {
    const collection = ANSM_COLLECTION;
    const { newRecords, oldRecords } = await filterRecordsByExistence({
        collection,
        records,
        filterField: FILTER_FIELD
    });

    if (newRecords.length) {
        await asyncSpinner({
            promise: create({ collection, payload: newRecords }),
            message: `Creating ${newRecords.length} records`
        });
    }

    if (oldRecords.length) {
        await asyncSpinner({
            promise: Promise.all(oldRecords.map((record) => updateOne({ collection, record }))),
            message: `Updating ${oldRecords.length} records`
        });
    }
}

function sanitizeRecords(records) {
    return records.map(({ id, fields }) => ({ id, ...fields }));
}

const {
    ANSM_DATABASE_ID,
    ANSM_MAIN_DATABASE_NAME,
    ANSM_AUTHORITY_DATABASE_NAME,
    ANSM_NEEDS_DATABASE_NAME,
    ANSM_SOLUTIONS_DATABASE_NAME,
} = process.env;

const COMMAND = 'ansm';
const INFO = `
    ANSM command fetches data from a table to export it to the main API.
    Once saved, it updates search engine documents according to the new data.

    In search mode it will only update search engine from current API data.

    In default mode (read), it provides statistics about table, API and search engine.
`;
const CLOSED_STATUS = '{🚩 Statut} = "👏 ANSM clos"';
export const ANSM_COLLECTION = 'ansm';
const FILTER_FIELD = 'local_authority';
const FIELDS_WHITELIST = [
    "🚩 Statut",
    "📍 Collectivité",
    "🤖 Attentes",
    "🌑 Besoin 1",
    "🌑 Besoin 2",
    "🌑 Besoin 3",
    "🌑 Besoin 4",
    "🌑 Besoin 5",
    "☀️ Solutions 1",
    "☀️ Solutions 2",
    "☀️ Solutions 3",
    "☀️ Solutions 4",
    "☀️ Solutions 5",
    "☎️ Téléphone",
    "✉  Email",
];
const CONVERSION_TABLE = {
    "🚩 Statut": "status",
    "📌 Collectivité": "local_authority",
    "🤖 Attentes": "expectations",
    "🌑 Besoin 1": "need_a",
    "🌑 Besoin 2": "need_b",
    "🌑 Besoin 3": "need_c",
    "🌑 Besoin 4": "need_d",
    "🌑 Besoin 5": "need_e",
    "☀️ Solutions 1": "solution_a",
    "☀️ Solutions 2": "solution_b",
    "☀️ Solutions 3": "solution_c",
    "☀️ Solutions 4": "solution_d",
    "☀️ Solutions 5": "solution_e",
    "✉️ Code postal": "zip_code",
    "🛖 EPCI": "epci",
    "🏫 Département": "department",
    "👤 Population": "population",
    "☎️ Téléphone": "phone",
    "✉  Email": "email",
    "🚜 Programme ANCT": "programs",
    "🟦 Région": "region"
}

const NEED_DECISION = {
    databaseName: ANSM_NEEDS_DATABASE_NAME,
    formatOutput: ({ key, output }) => ({
        [key]: output.map(({ fields }) => fields["🌑 Besoins exprimés"])
    }),
};
const SOLUTION_DECISION = {
    databaseName: ANSM_SOLUTIONS_DATABASE_NAME,
    formatOutput: ({ key, output }) => ({
        [key]: output.map(({ fields }) => ({
            name: fields["Nom du service"],
            editor: fields["Porteurs"],
            isOpenSource: fields["Open-source ?"] === "Oui",
            url: fields["Lien vers le service - cp"]
        }))
    })
};
const DATA_DECISION_TREE = {
    "📍 Collectivité": {
        databaseName: ANSM_AUTHORITY_DATABASE_NAME,
        formatOutput:  ({ output }) => {
            const [{ fields }] = output;
            return { ...fields }
        },
    },
    "🌑 Besoin 1": NEED_DECISION,
    "🌑 Besoin 2": NEED_DECISION,
    "🌑 Besoin 3": NEED_DECISION,
    "🌑 Besoin 4": NEED_DECISION,
    "🌑 Besoin 5": NEED_DECISION,
    "☀️ Solutions 1": SOLUTION_DECISION,
    "☀️ Solutions 2": SOLUTION_DECISION,
    "☀️ Solutions 3": SOLUTION_DECISION,
    "☀️ Solutions 4": SOLUTION_DECISION,
    "☀️ Solutions 5": SOLUTION_DECISION,
}
