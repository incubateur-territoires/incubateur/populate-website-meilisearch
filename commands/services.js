import { displayCommandTitle } from "../lib/cli.js";
import { logInfo } from "../lib/logger.js";
import { getSearchEngineCollection } from "../lib/search.js";
import { asyncSpinner } from "../lib/spinner.js";
import { find } from "../lib/api.js"

export default async function command(mode) {
    if (mode === 'info') {
        displayInfo(INFO);
    }

    displayCommandTitle({ command: COMMAND, mode });

    const apiRecords = await asyncSpinner({ 
        promise: find({
            collection: SERVICES_COLLECTION,
            params: {
                filter: {
                    status: {
                        '_eq': 'published'
                    }
                }
            }
        }),
        message: 'Fetching records from API',
    });

    logInfo(`API documents number: ${apiRecords.length}`);

    const client = getSearchEngineCollection(SERVICES_COLLECTION);

    if (mode !== 'read') {
        await asyncSpinner({
            promise: client.deleteAllDocuments(),
            message: 'Delete all documents from search engine',
        });

        await asyncSpinner({
            promise: client.addDocuments(apiRecords),
            message: 'Adding fresh documents into search engine',
        });
    }

    const { numberOfDocuments } = await client.getStats();

    logInfo(`Search engine documents number: ${numberOfDocuments}`);
    process.exit(0);
}

const COMMAND = 'services';
const INFO = `
    ANSM command fetches data from API to export to search engine.
    In default mode (read), it provides statistics about API and search engine.
`;
export const SERVICES_COLLECTION = 'services';