#! /usr/bin/env node

import 'dotenv/config';
import { parseCLI, runCommand } from '../lib/cli.js';

runCommand(parseCLI());
